# Kleber Correia
## Enqueue new Scrape

```bash
curl -XPOST http://kleber.hiring.keywee.io/stories\?url\=http://www.bikeroar.com \
  -d "{}" \
  -H "content-type: application/json" \
  -w "\n"

{"id":"b0e0dca8-8e84-4318-9f7e-754e0585f8b3"}
```

## Check status

```bash
curl http://kleber.hiring.keywee.io/stories/b0e0dca8-8e84-4318-9f7e-754e0585f8b3 -s | jq

{
  "id": "b0e0dca8-8e84-4318-9f7e-754e0585f8b3",
  "url": "http://www.bikeroar.com",
  "scrape_status": "pending"
}

# or

{
  "id": "597306ae-14b7-419f-8535-68d0ed998e59",
  "url": "http://www.bikeroar.com.br",
  "scrape_status": "error"
}
```

```bash
curl http://kleber.hiring.keywee.io/stories/b0e0dca8-8e84-4318-9f7e-754e0585f8b3 -s | jq

{
  "id": "2a931d48-1f32-4554-a836-cc26a5367184",
  "url": "http://www.bikeroar.com",
  "type": "website",
  "title": "Find Bikes, Local Bike Shops & Awesome Cycling Advice - BikeRoar",
  "updated_time": "2021-02-01T11:08:08.103Z",
  "scrape_status": "done",
  "images": [
    {
      "url": "http://assets.bikeroar.com/static/images/BR_logo_horizontal_1500x750_left.jpg",
      "image_type": "image/jpeg",
      "width": null,
      "height": null,
      "alt": null
    }
  ]
}
```

## Architecture

Simple JSON ReST API, which exposes fow routes:

```
rails routes
     Prefix Verb URI Pattern            Controller#Action
       root GET  /                      healthcheck#healthcheck
sidekiq_web      /sidekiq               Sidekiq::Web
healthcheck GET  /healthcheck(.:format) healthcheck#healthcheck
    og_tags POST /stories(.:format)     og_tags#create
     og_tag GET  /stories/:id(.:format) og_tags#show
```

Each valid POST request made to `/stories` will be processed in background via `sidekiq`.
## Deploy

I've deployed Nginx as a load balancer/reverse proxy to the Rails Application.

Many useful commands can be found in the `Makefile`.

### Containers

This project uses Docker to containerize the app and its dependencies. See `./Dockerfile`, `./docker-compose.yml`, and `docker-compose.prod.yml`

### Ansible

The old but gold, Ansible has been used to set up the basics in the server. See `./deploy` folder.
## Stuff I haven't done

1. I skipped tests in this challenge, with more time I'd spend more time writing tests
2. JSON Serializer/Presenter - ATM, the responses are explicitly built in the controller, with more time I'd use jbuilder and/or create a presenter to represent all possible responses.
3. Decent commit messages
