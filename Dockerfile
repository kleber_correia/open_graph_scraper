FROM ruby:3.0.0-alpine as builder

RUN apk --no-cache add \
      build-base \
      curl \
      curl-dev \
      git \
      libffi-dev \
      linux-headers \
      postgresql-dev \
      ruby-dev \
      tzdata \
      wget \
      zlib-dev

WORKDIR /app
COPY Gemfile .
RUN touch Gemfile.lock

RUN bundle config --global path "/gems"
ARG RAILS_ENV
RUN [[ "$RAILS_ENV" == "production" ]] && bundle config --global without "development:test"
RUN bundle install

FROM ruby:3.0.0-alpine

RUN apk --no-cache add \
    libpq \
    procps \
    tzdata

RUN bundle config --global path "/gems"
COPY --from=builder /gems /gems
ARG RAILS_ENV
RUN [[ "$RAILS_ENV" == "production" ]] && bundle config --global without "development:test"

WORKDIR /app
COPY . /app
COPY --from=builder /app/Gemfile.lock Gemfile.lock


