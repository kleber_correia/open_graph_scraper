# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  root to: 'healthcheck#healthcheck'

  mount Sidekiq::Web => '/sidekiq'

  get '/healthcheck', to: 'healthcheck#healthcheck'

  resources :og_tags, path: :stories, only: %i[create show]
end
