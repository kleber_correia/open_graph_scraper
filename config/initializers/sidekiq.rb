# frozen_string_literal: true

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://redis:6379/0', id: "Sidekiq-server-PID-#{::Process.pid}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://redis:6379/0', id: "Sidekiq-server-PID-#{::Process.pid}" }
end
