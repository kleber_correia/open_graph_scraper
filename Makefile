up:
	@scp docker-compose.prod.yml ubuntu@kleber.hiring.keywee.io:/tmp/docker-compose.prod.yml
	@ssh ubuntu@kleber.hiring.keywee.io sudo docker-compose -f /tmp/docker-compose.prod.yml down
	@ssh ubuntu@kleber.hiring.keywee.io sudo docker-compose -f /tmp/docker-compose.prod.yml up -d --remove-orphans

rails:
	@ssh ubuntu@kleber.hiring.keywee.io sudo docker-compose -f /tmp/docker-compose.prod.yml run api /app/bin/rails console -e production

build:
	@docker --context=keywee build . -t scraper:latest --build-arg RAILS_ENV=production

logs:
	@ssh ubuntu@kleber.hiring.keywee.io sudo docker-compose -f /tmp/docker-compose.prod.yml logs -f

lint:
	@bundle exec rubocop -A --require rubocop-rails
