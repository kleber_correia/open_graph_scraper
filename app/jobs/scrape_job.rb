# frozen_string_literal: true

class ScrapeJob < ApplicationJob
  sidekiq_options retry: false
  queue_as :default

  def perform(tag)
    ActiveRecord::Base.transaction do
      OgScraper.call(tag)
    end
  end
end
