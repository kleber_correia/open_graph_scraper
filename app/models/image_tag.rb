# frozen_string_literal: true

class ImageTag < ApplicationRecord
  belongs_to :tag

  validates :url, presence: true
end
