# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :image_tags, dependent: :destroy
  enum status: { 'pending' => 0, 'done' => 1, 'error' => 2 }

  attr_accessor :image

  validates :url, presence: true, format: URI::DEFAULT_PARSER.make_regexp
end
