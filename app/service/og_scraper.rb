# frozen_string_literal: true

class OgScraper
  def self.call(tag)
    new(tag).call
  end

  def initialize(tag)
    @tag = tag
  end

  def call
    html = HttpClient.call(tag.url)
    add_basic_og_metadata(tag, html)
    add_image_og_metadata!(tag, html) if tag.image
    tag.done!
  rescue HttpClient::HTTPError => e
    Rails.logger.error(e.message)
    tag.error!
  rescue ActiveRecord::RecordInvalid => e
    Rails.logger.error(e.message)
    tag.error!
  end

  private

  attr_accessor :tag

  def add_basic_og_metadata(tag, html)
    basic = Og::BasicParser.new(html)
    tag.og_type = basic.type
    tag.title = basic.title
    tag.url = basic.url || tag.url
    tag.image = basic.image
  end

  def add_image_og_metadata!(tag, html)
    img = Og::ImageParser.new(html)
    tag.image_tags.create!(
      url: img.url || tag.image,
      width: img.width.presence,
      image_type: img.type,
      height: img.height,
      alt: img.alt
    )
  end
end
