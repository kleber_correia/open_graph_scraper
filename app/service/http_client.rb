# frozen_string_literal: true

require 'open-uri'

module HttpClient
  module_function

  HTTPError = Class.new(StandardError)

  def call(url)
    raise HTTPError, 'invalid url' unless URI::DEFAULT_PARSER.make_regexp.match?(url)

    URI.open(url).read
  rescue OpenURI::HTTPError => e
    raise HTTPError, e.message
  rescue SocketError
    raise HTTPError, 'failed to open url'
  rescue StandardError
    raise HTTPError, 'unknown'
  end
end
