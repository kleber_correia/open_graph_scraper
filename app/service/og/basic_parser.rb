# frozen_string_literal: true

module Og
  class BasicParser < Base
    def type = content_for(:type)

    def title = content_for(:title)

    def url = content_for(:url)

    def image = content_for(:image)

    private

    def content_for(property)
      super(%(meta[property="og:#{property}"]))
    end
  end
end
