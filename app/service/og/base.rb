# frozen_string_literal: true

module Og
  class Base
    attr_reader :document

    def initialize(raw_html, html_parser = Nokogiri.method(:HTML))
      @document = html_parser.call(raw_html)
    end

    private

    def content_for(css_selector)
      document.css(css_selector).first&.attributes&.fetch('content', nil)&.presence
    end
  end
end
