# frozen_string_literal: true

module Og
  class ImageParser < Base
    def width = content_for(:width)

    def height = content_for(:height)

    def url = content_for(:url)

    def alt = content_for(:alt)

    def type = content_for(:type)

    private

    def content_for(property)
      super(%(meta[property="og:image:#{property}"]))
    end
  end
end
