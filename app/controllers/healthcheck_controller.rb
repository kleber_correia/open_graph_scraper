# frozen_string_literal: true

class HealthcheckController < ApplicationController
  def healthcheck
    render plain: File.read(Rails.root.join('README.md'))
  end
end
