# frozen_string_literal: true

class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordInvalid, with: :show_errors
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  private

  def show_errors(exception)
    render json: exception.record.errors.as_json, status: :unprocessable_entity
  end

  def not_found(_)
    render json: {}, status: :not_found
  end
end
