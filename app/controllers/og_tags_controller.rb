# frozen_string_literal: true

class OgTagsController < ApplicationController
  def create
    tag = Tag.create!(url: params[:url])
    ScrapeJob.perform_later(tag)
    render json: { id: tag.id }
  end

  def show
    unless tag.done?
      return render json: {
        id: tag.id,
        url: tag.url,
        scrape_status: tag.status
      }
    end

    render json: {
      id: tag.id,
      url: tag.url,
      type: tag.og_type,
      title: tag.title,
      updated_time: tag.updated_at,
      scrape_status: tag.status,
      images: tag.image_tags.map do |image_tag|
        image_tag.as_json(only: %i[url image_type width height alt])
      end
    }
  end

  def tag
    @tag ||= Tag.find(params[:id])
  end
end
