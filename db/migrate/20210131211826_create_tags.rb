# frozen_string_literal: true

class CreateTags < ActiveRecord::Migration[6.1]
  def change
    create_table :tags, id: :uuid do |t|
      t.string :url
      t.string :og_type
      t.string :title
      t.integer :status, null: false, default: 0

      t.timestamps
    end
    add_index :tags, :og_type
    add_index :tags, :status
  end
end
