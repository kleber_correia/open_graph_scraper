# frozen_string_literal: true

class CreateImageTags < ActiveRecord::Migration[6.1]
  def change
    create_table :image_tags do |t|
      t.string :url, null: false
      t.string :image_type
      t.integer :width
      t.integer :height
      t.string :alt
      t.references :tag, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
